# Scripts for small jobs

Here there are some scripts to do small jobs

* clean_watermak.sh:
  
  * detects eight digit watermark in pdfs and deletes them. Then extract the text on them.
    
    * uses `pdftk`, `pdftotext`, `find`, `sed` and `grep`.
      To use, first make it executable.
      
      ```bash
      chmod +x clean_watermark.sh
      ```
      
      ```bash
      ./clean_watermark /path/to/pdf_folder/
      ```
