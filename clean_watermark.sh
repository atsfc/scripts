#!/bin/bash
# Watermark cleaning script for PDFs
# version=0.9

# Check if folder argument provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <folder>"
    exit 1
fi

# Ensure folder exists
folder="$1"
if [ ! -d "$folder" ]; then
    echo "Error: Folder '$folder' not found."
    exit 1
fi

# Function to remove watermark and extract the text
remove_watermark() {
    local input_pdf="$1"
    
    # Check if input PDF exists
    if [ ! -f "$input_pdf" ]; then
        echo "Error: Input PDF file '$input_pdf' does not exist."
        return 1
    fi

    # Temporary directory
    local tempdir=$(mktemp -d -p /tmp)
    
    # Uncompress the PDF (assuming pdftk can uncompress)
    pdftk "$input_pdf" output "$tempdir/temp.pdf" uncompress >/dev/null 2>&1

    # Detect and remove watermark text using sed
    # the following regex searches for any digit numbers repeats with the pattern "digit - "
    # than deletes the whole line
    sed -r -i.bak "/(([0-9]+).-.){5}/d" "$tempdir/temp.pdf"

    # Check if sed command was successful (exit code 0 indicates success)
    if [ $? -ne 0 ]; then
      echo "Error: Failed to remove watermark text."
      mv "$tempdir/temp.pdf.bak" "$tempdir/temp.pdf"  # Restore backup if sed failed
      return 1
    fi
    
    # Convert PDF to text
    if ! pdftotext -layout "$tempdir/temp.pdf" "$tempdir/temp.txt"; then
        echo "Error: Failed to extract text from '$input_pdf'."
        return 1
    fi

    # Move extracted text file
    mv "$tempdir/temp.txt" "${input_pdf%.pdf}.txt"

    # Remove the temporary directory
    rm -rf "${tempdir}"
}

# Process PDF files
count=0
while IFS= read -r -d '' file; do
    remove_watermark "$file" && ((count++))
done < <(find "$folder" -maxdepth 1 -type f -name '*.pdf' -print0)

echo "Number of processed PDFs: $count."

